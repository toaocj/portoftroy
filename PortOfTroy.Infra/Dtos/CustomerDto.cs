﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PortOfTroy.Infra.Dtos
{
        public class CustomerDto : BaseDto
        {
        public int? ParkId { get; set; }
        public int? BungalowId { get; set; }
        public int? UserId { get; set; }
        public int? ReservationId { get; set; }
        public string Name { get; set; }
    }
    
}
