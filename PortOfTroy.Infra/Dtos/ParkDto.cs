﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PortOfTroy.Infra.Dtos
{
    public class ParkDto : BaseDto
    {
        public string Name { get; set; }
        public string Country { get; set; }

        /* Later wanneer de FKs werken
        public List<Bungalow> Bungalows { get; set; }
        public List<Employee> Employees { get; set; }
        public List<Customer> Customers { get; set; }
        public Park()
        {
            Bungalows = new List<Bungalow>();
            Employees = new List<Employee>();
            Customers = new List<Customer>();
        }
        */
    }
}
