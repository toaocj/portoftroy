﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PortOfTroy.Infra.Dtos
{
        public class UserDto : BaseDto
        {
        public int? InvoiceId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }

        public string Email { get; set; }
        public string Password { get; set; }

        /* Plaintext je passwords opslaan is zeer gevaarlijk, maar passwordhashes en salts zijn een beetje te ingewikkeled en veel moeite voor een school project
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; } */

        public CustomerDto Customer { get; set; }

        public UserDto()
        {
            Customer = new CustomerDto();
        }

    }
    
}
