﻿using PortOfTroy.Backend.Entities;
using PortOfTroy.Infra.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortOfTroy.Backend.Mappers
{
    public class InvoiceMapper : IMapper<Invoice, InvoiceDto>
    {
        public Invoice MapFromDto(InvoiceDto invoiceDto)
        {
            var Invoice = new Invoice()
            {
                Id = invoiceDto.Id,
                Adress = invoiceDto.Adress
            };
            return Invoice;
        }

        public void MapFromDto(Invoice invoice, InvoiceDto invoiceDto)
        {
            invoice.Id = invoiceDto.Id;
            invoice.Adress = invoiceDto.Adress;
        }

        public InvoiceDto MapToDto(Invoice invoice)
        {
            var dto = new InvoiceDto()
            {
                Id = invoice.Id,
                Adress = invoice.Adress
            };
            return dto;
        }
    }

}
