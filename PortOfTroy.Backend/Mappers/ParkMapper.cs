﻿using PortOfTroy.Backend.Entities;
using PortOfTroy.Infra.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortOfTroy.Backend.Mappers
{
    public class ParkMapper : IMapper<Park, ParkDto>
    {
        public Park MapFromDto(ParkDto parkDto)
        {
            var Park = new Park()
            {
                Id = parkDto.Id,
                Name = parkDto.Name,
                Country = parkDto.Country
            };
            return Park;
        }

        public void MapFromDto(Park park, ParkDto parkDto)
        {
            park.Id = parkDto.Id;
            park.Name = parkDto.Name;
            park.Country = parkDto.Country;
        }

        public ParkDto MapToDto(Park park)
        {
            var dto = new ParkDto()
            {
                Id = park.Id,
                Name = park.Name,
                Country = park.Country
            };
            return dto;
        }
    }

}
