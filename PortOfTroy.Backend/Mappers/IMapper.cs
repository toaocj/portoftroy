﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortOfTroy.Backend.Mappers
{
    public interface IMapper<M, D>
    {
        M MapFromDto(D sortingMachineDto);
        void MapFromDto(M sortingMachine, D sortingMachineDto);
        D MapToDto(M domain);
    }
}
