﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PortOfTroy.Backend.Migrations
{
    public partial class NoBungalowInInvoice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BungalowId",
                table: "Invoice");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BungalowId",
                table: "Invoice",
                type: "int",
                nullable: true);
        }
    }
}
