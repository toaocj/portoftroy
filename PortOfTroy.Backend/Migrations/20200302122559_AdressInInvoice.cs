﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PortOfTroy.Backend.Migrations
{
    public partial class AdressInInvoice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Adress",
                table: "Invoice",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Adress",
                table: "Invoice");
        }
    }
}
