﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PortOfTroy.Backend.Migrations
{
    public partial class TempMig : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_User_Invoice_InvoiceId",
                table: "User");

            migrationBuilder.DropIndex(
                name: "IX_User_InvoiceId",
                table: "User");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_User_InvoiceId",
                table: "User",
                column: "InvoiceId",
                unique: true,
                filter: "[InvoiceId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_User_Invoice_InvoiceId",
                table: "User",
                column: "InvoiceId",
                principalTable: "Invoice",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
