﻿using PortOfTroy.Backend.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortOfTroy.Backend.Services
{
    public class Service<T> : IService<T>
    {
        private readonly IGenericRepository<T> _genericrepository;
        public Service(IGenericRepository<T> genericRepository)
        {
            _genericrepository = genericRepository;
        }
        public virtual async Task<T> GetById(int id)
        {
            return await _genericrepository.GetById(id);
        }

        public virtual async Task<IList<T>> GetAll()
        {
            return await _genericrepository.GetAll();
        }

        public virtual async Task Create(T entity)
        {
            await _genericrepository.Create(entity);
        }

        public virtual async Task Update(T entity)
        {
            await _genericrepository.Update(entity);
        }

        public virtual async Task Remove(T entity)
        {
            await _genericrepository.Remove(entity);
        }
    }
}
