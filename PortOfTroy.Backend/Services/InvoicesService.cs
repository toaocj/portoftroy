﻿using PortOfTroy.Backend.Entities;
using PortOfTroy.Backend.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortOfTroy.Backend.Services
{
    public class InvoicesService : Service<Invoice>, IInvoicesService
    {
        private readonly IInvoicesRepository _repository;
        public InvoicesService(IInvoicesRepository repository) : base(repository)
        {
            _repository = repository;
        }

    }
}
