﻿using PortOfTroy.Backend.Entities;
using PortOfTroy.Backend.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortOfTroy.Backend.Services
{
    public class ParksService : Service<Park>, IParksService
    {
        private readonly IParksRepository _repository;
        public ParksService(IParksRepository repository) : base(repository)
        {
            _repository = repository;
        }

    }
}
