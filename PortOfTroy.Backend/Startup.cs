using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using PortOfTroy.Backend;
using Microsoft.OpenApi.Models;
using PortOfTroy.Backend.Services;
using PortOfTroy.Backend.Repository;
using PortOfTroy.Infra.Dtos;
using PortOfTroy.Backend.Entities;
using PortOfTroy.Backend.Mappers;

namespace PortOfTroy.Backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region Services
            services.AddScoped<IInvoicesService, InvoicesService>();
            services.AddScoped<IParksService, ParksService>();
            #endregion

            #region Repositories
            services.AddScoped<IInvoicesRepository, InvoicesRepository>();
            services.AddScoped<IParksRepository, ParksRepository>();
            #endregion

            #region Mappers
            services.AddScoped<IMapper<Invoice, InvoiceDto>, InvoiceMapper>();
            services.AddScoped<IMapper<Park, ParkDto>, ParkMapper>();

            #endregion

            services.AddControllers();

            services.AddControllersWithViews();

            services.AddDbContext<PPContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "POT API", Version = "v1" });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Port Of Troy API");
                c.RoutePrefix = string.Empty;
            });
        }

        /*
        protected override void OnModelCreating(ModelBuilder builder)
        {
            var cascadeFKs = builder.Model.GetEntityTypes()
                .SelectMany(t => t.GetForeignKeys())
                .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs)
                fk.DeleteBehavior = DeleteBehavior.Restrict;
        }
        */
        }
}
