﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PortOfTroy.Backend.Entities;
using PortOfTroy.Backend.Mappers;
using PortOfTroy.Backend.Repository;
using PortOfTroy.Backend.Services;
using PortOfTroy.Infra.Dtos;

namespace PortOfTroy.Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ParksController : ControllerBase
    {

        private readonly ILogger<ParksController> _logger;
        private readonly IMapper<Park, ParkDto> _mapper;
        private readonly IParksService _service;
        private readonly IParksRepository _repository;

        public ParksController(PPContext context,
            ILogger<ParksController> logger,
            IMapper<Park, ParkDto> mapper,
            IParksService service,
            IParksRepository repository)
        {
            _logger = logger;
            _service = service;
            _mapper = mapper;
            _repository = repository;


        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ParkDto>>> Get()
        {
            var parks = await _service.GetAll();

            var parkDtos = new List<ParkDto>();

            foreach (var park in parks)
            {
                var parkDto = _mapper.MapToDto(park);
                parkDtos.Add(parkDto);
            }


            return parkDtos;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ParkDto>> GetById(int id)
        {
            var parkFromDb = await _service.GetById(id);
            if (parkFromDb == null)
                return NotFound();

            return Ok(_mapper.MapToDto(parkFromDb));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var parkInDb = await _service.GetById(id);
            if (parkInDb == null)
                return NotFound();

            await _service.Remove(parkInDb);
            return Ok();
        }

        [HttpPut]
        public async Task<ActionResult<ParkDto>> Put(ParkDto parkDto)
        {
            var park = _mapper.MapFromDto(parkDto);

            await _service.Update(park);

            //todo check null
            return Ok(_mapper.MapToDto(park));
        }

        [HttpPost]
        public async Task<ActionResult<ParkDto>> Post(ParkDto parkDto)
        {
            var park = _mapper.MapFromDto(parkDto);

            await _service.Create(park);

            return Ok(_mapper.MapToDto(park));
        }
    }
}