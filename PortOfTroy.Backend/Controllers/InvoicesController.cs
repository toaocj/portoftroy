﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PortOfTroy.Backend.Entities;
using PortOfTroy.Backend.Mappers;
using PortOfTroy.Backend.Repository;
using PortOfTroy.Backend.Services;
using PortOfTroy.Infra.Dtos;

namespace PortOfTroy.Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class InvoicesController : ControllerBase
    {

        private readonly ILogger<InvoicesController> _logger;
        private readonly IMapper<Invoice, InvoiceDto> _mapper;
        private readonly IInvoicesService _service;
        private readonly IInvoicesRepository _repository;

        public InvoicesController(PPContext context,
            ILogger<InvoicesController> logger,
            IMapper<Invoice, InvoiceDto> mapper,
            IInvoicesService service,
            IInvoicesRepository repository)
        {
            _logger = logger;
            _service = service;
            _mapper = mapper;
            _repository = repository;


        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<InvoiceDto>>> Get()
        {
            var invoices = await _service.GetAll();

            var invoiceDtos = new List<InvoiceDto>();

            foreach (var invoice in invoices)
            {
                var invoiceDto = _mapper.MapToDto(invoice);
                invoiceDtos.Add(invoiceDto);
            }


            return invoiceDtos;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<InvoiceDto>> GetById(int id)
        {
            var invoiceFromDb = await _service.GetById(id);
            if (invoiceFromDb == null)
                return NotFound();

            return Ok(_mapper.MapToDto(invoiceFromDb));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var invoiceInDb = await _service.GetById(id);
            if (invoiceInDb == null)
                return NotFound();

            await _service.Remove(invoiceInDb);
            return Ok();
        }

        [HttpPut]
        public async Task<ActionResult<InvoiceDto>> Put(InvoiceDto invoiceDto)
        {
            var invoice = _mapper.MapFromDto(invoiceDto);

            await _service.Update(invoice);

            //todo check null
            return Ok(_mapper.MapToDto(invoice));
        }

        [HttpPost]
        public async Task<ActionResult<InvoiceDto>> Post(InvoiceDto invoiceDto)
        {
            var invoice = _mapper.MapFromDto(invoiceDto);

            await _service.Create(invoice);

            return Ok(_mapper.MapToDto(invoice));
        }
    }
}
