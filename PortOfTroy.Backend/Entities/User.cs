﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PortOfTroy.Backend.Entities
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public int Id { get; set; }

        public int? InvoiceId { get; set; }

        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Username { get; set; }

        public string Email { get; set; }
        public string Password { get; set; }

        /* Plaintext je passwords opslaan is zeer gevaarlijk, maar passwordhashes en salts zijn een beetje te ingewikkeled en veel moeite voor een school project
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; } */

        public Customer Customer { get; set; }

        public User()
        {
            Customer = new Customer();
        }

    }
}
