﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PortOfTroy.Backend.Entities
{
    public class Park
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public List<Bungalow> Bungalows{ get; set; }
        public List<Employee> Employees { get; set; }
        public List<Customer> Customers { get; set; }
        public Park()
        {
            Bungalows = new List<Bungalow>();
            Employees = new List<Employee>();
            Customers = new List<Customer>();
        }
    }
}