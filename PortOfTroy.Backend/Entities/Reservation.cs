﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PortOfTroy.Backend.Entities
{
    public class Reservation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public int Id { get; set; }
        public List<Customer> Customers { get; set; }
        [Required]
        public int BungalowId { get; set; }

        public Reservation()
        {
            Customers = new List<Customer>();
        }
    }
}