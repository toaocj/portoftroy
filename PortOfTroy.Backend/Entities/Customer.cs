﻿
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PortOfTroy.Backend.Entities
{
    public class Customer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public int Id { get; set; }
        public int? ParkId { get; set; }
        public int? BungalowId { get; set; }
        public int? UserId { get; set; }
        public int? ReservationId { get; set; }
        public string Name { get; set; }


    }
}