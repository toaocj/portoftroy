﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PortOfTroy.Backend.Entities
{
    public class Bungalow
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public int Id { get; set; }
        public List<Customer> Customers { get; set; }
        public List<Reservation> Reservations { get; set; }
        [Required]
        public int? ParkId { get; set; }

        public Bungalow()
        {
            Reservations = new List<Reservation>();
            Customers = new List<Customer>();
        }

    }
}