﻿using PortOfTroy.Backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortOfTroy.Backend.Repository
{
    public interface IParksRepository : IGenericRepository<Park>
    {
    }
}
