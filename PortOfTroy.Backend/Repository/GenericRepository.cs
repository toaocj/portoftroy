﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortOfTroy.Backend.Repository
{
    public class GenericRepository<T> : IGenericRepository<T>
        where T : class
    {
        protected DbSet<T> _entity;
        protected PPContext _context;

        public GenericRepository(PPContext context)
        {
            _context = context;
            _entity = context.Set<T>();
        }

        public virtual async Task<T> GetById(int id)
        {
            return await _entity.FindAsync(id);
        }

        public virtual async Task<IList<T>> GetAll()
        {
            return await _entity.ToListAsync();
        }

        public virtual async Task Create(T entity)
        {
            await _entity.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public virtual async Task Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public virtual async Task Remove(T entity)
        {
            _entity.Remove(entity);
            await _context.SaveChangesAsync();
        }
    }
}
