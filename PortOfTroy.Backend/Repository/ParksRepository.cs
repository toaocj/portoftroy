﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using PortOfTroy.Backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortOfTroy.Backend.Repository
{
    public class ParksRepository : GenericRepository<Park>, IParksRepository
    {
        public ParksRepository(PPContext context) : base(context)
        {
        }
    }

}
