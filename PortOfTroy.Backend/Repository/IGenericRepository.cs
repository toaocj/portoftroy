﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortOfTroy.Backend.Repository
{
    public interface IGenericRepository<T>
    {
        Task<T> GetById(int id);
        Task<IList<T>> GetAll();
        Task Create(T entity);
        Task Update(T entity);
        Task Remove(T entity);
        //Task SaveChangesAsync();
    }
}
